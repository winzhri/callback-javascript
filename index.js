// 1

const Table = document.getElementById("listUser");
Table.innerHTML = loading();

// 3

function render(result) {
  let Table = "";
  result.forEach((listUser) => {
    Table += `
					<tr>
						<td> ${listUser.id} </td>
						<td> ${listUser.name} </td>
						<td> ${listUser.username} </td>
						<td> ${listUser.email} </td>
						<td> ${listUser.phone} </td>
          				<td>
							${listUser.address.street},
							${listUser.address.suite},
							${listUser.address.city}
						</td>
						<td> ${listUser.company.name} </td>
					</tr>`;
  });
  return Table;
}

// 4

fetch("https://jsonplaceholder.typicode.com/users")
  .then((res) => res.json())
  .then((res) => {
    Table.innerHTML = render(res);
  });

// 2

function loading() {
  return `<tr colspan = "6" class="text-center"> Loading... </tr>`;
}
